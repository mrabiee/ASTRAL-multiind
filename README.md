# ASTRAL-multiind
ASTRAL is a tool for estimating an unrooted species tree given a set of unrooted gene trees. ASTRAL is statistically consistent under the multi-species coalescent model (and thus is useful for handling ILS). ASTRAL finds the species tree that has the maximum number of shared induced quartet trees with the set of gene trees. 
The multi-individual algorithm is described in:

* Rabiee, Maryam, Erfan Sayyari, and Siavash Mirarab. "Multi-allele species reconstruction using ASTRAL." bioRxiv (2018): 439489.

Data:
-----------
Some of the code and data used in the paper have been uploaded here on GitLab, the rest can be found [here](https://maryamrabiee.github.io/ASTRAL-multi/).
 
